const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

// source: https://tsmx.net/jest-process-exit/
it('tests sum with process.exit', async () => {
  const mockExit = jest.spyOn(process, 'exit')
      .mockImplementation((number) => { throw new Error('process.exit: ' + number); });
  expect(() => {
      sum("a", 1);
  }).toThrow();
  expect(mockExit).toHaveBeenCalledWith(1);
  mockExit.mockRestore();
});
