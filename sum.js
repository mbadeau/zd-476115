function sum(a, b) {
  if (Number.isInteger(a + b)) {
    return a + b;
  }
  process.exit(1);
}
module.exports = sum;
